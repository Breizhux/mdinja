# Mdinja

Create a jinja markdown template, and build it to .md and .pdf with your own data.
Very simple tool.

## Installation

### On Linux :

In superuser :
```
apt update
apt install -y pandoc wkhtmltopdf
```

In your favorite folder :
```
virtualenv ./
source bin/activate
pip install git+https://gitlab.com/Breizhux/mdinja.git
```


## Run

### Cli mode :

**Warning :** Watch the order of arguments.

```
#if necessary :
source bin/activate #in the good directory

#datas from json file
mdinja -i template.md -o "my_auto_pdf.pdf" "ex_data.json"

#datas directly in command line
mdinja -i template.md '{"title": "coucou", "code": true, "line_code": "result = 2+2", "content": "Ceci est le contenu du document !"}'

#one shot generate multiple pdf
mdinja -i template.md -o my_output --delete-md '[{"title": "coucou", "code": true, "line_code": "result = 2+2", "content": "Ceci est le contenu du document !"}, {"title": "2", "code": true, "line_code": "result = 3+3", "content": "content of doc"}]'
```

### Use in your python code :

```
import mdinja

filename = "path_to_template.md"
datas = {
    "my_data" : "values"
}

#build your template with your datas
redering = mdinja.rendering_engine(filename, datas, md_output="output.md")

#convert your new md file into pdf
mdinja.md_to_pdf("output.md", css_file="modest")
```


## Informations

**CSS** come from the default style of github and [this repository](https://github.com/markdowncss).
