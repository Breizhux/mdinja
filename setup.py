#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import setuptools

setuptools.setup(
    name = 'mdinja',
    version = '1.0.0',
    description = 'Outil de création de PDF à partir d\'un markdown avec syntaxe jinja2',
	#license = "WTFPL",
    url = 'https://gitlab.com/breizhux/mdinja',
    author = 'Breizhux',
    author_email = 'xavier.lanne@gmx.fr',
    packages = setuptools.find_packages(
        exclude = [
            'bin',
            'lib',
            '__pycache__',
            'pyvenv.cfg',
            '.gitignore',
            'requirements.txt',
        ],
    ),
    package_data = {'mdinja' : [
        'styles/*',
    ]},
    include_package_data=True,
    install_requires = [
        'jinja2'
    ],
	python_requires = ">=3.8",
    entry_points = {
        'console_scripts': [
            'mdinja  =  mdinja.mdinja:main',
        ],
    },
)
