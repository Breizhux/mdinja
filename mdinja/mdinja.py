import os
import sys
import json
import subprocess
from jinja2 import Template


def rendering_engine(filename, data, md_output=None) :
    """ Build your jinja2 markdown template with your datas.
    md_output : optionnal file path to write result."""
    #read template
    with open(filename, 'r') as file :
        md_template = file.read()
    #build template
    template = Template(md_template)
    rendered_text = template.render(**data)
    #save and return
    if md_output is not None :
        #md_output = f"{input_file[:-3]}-build.md"
        with open(md_output, 'w') as file :
            file.write(rendered_text)
    return rendered_text


def md_to_pdf(input_file, pdf_output=None, css_file=None, delete_md=False) :
    """ Run this command :
    pandoc output.md --css modest.css -s -o output.pdf --pdf-engine=wkhtmltopdf
    input_file : markdown input file to convert
    pdf_output (opt) : output pdf filename (default : same as input, but in .pdf)
    css_file (opt) : css stylesheet to user for generate pdf.
    """
    if pdf_output is None :
        pdf_output = f"{input_file[:-3]}.pdf"
    #create command line
    command = f"pandoc \"{input_file}\" -o \"{pdf_output}\" --pdf-engine=wkhtmltopdf "
    if css_file is not None :
        if css_file in ["github", "air", "modest", "retro", "splendor"] :
            css_file = f"styles/{css_file}.css"
        if not os.path.exists(css_file) :
            if os.path.exists(f"styles/{css_file}") :
                css_file = f"styles/{css_file}"
            else :
                print("Erreur : le fichier CSS n'a pas pu être trouvé.")
                return 1
        command += f"--css {css_file} -s"
    #execute command line
    output = subprocess.getstatusoutput(command)
    if output[0] != 0 :
        print("Erreur : Une erreur est survenue lors de la conversion en pdf :")
        print("command :", command)
        print("sortie :", output[1])
        return 2
    if delete_md : os.remove(input_file)
    #return no error :)
    return 0


def build_multiple(options) :
    """ One shot build template."""
    for i, data in enumerate(options['json_data']) :
        md_output = f"{options['md_output'][:-3]}-{i+1}.md"
        pdf_output = f"{options['pdf_output'][:-4]}-{i+1}.pdf"

        #redering...
        result = rendering_engine(options['filename'],
                                  data,
                                  md_output=md_output)
        #...and converting
        md_to_pdf(md_output,
                  pdf_output=pdf_output,
                  css_file=options['css_file'],
                  delete_md=options['delete_md'] )


def parser(args) :
    """ Parse argument after the input file.
    The only not named argument is the json data : {...}"""
    options = {
        'filename' : None,
        'output' : "output",
        'md_output' : None,
        'pdf_output' : None,
        'css_file' : None,
        'json_data' : "",
        'delete_md' : False,
    }
    key, value = "", ""
    for i in args :
        #case of option after key : -i template.md
        if key != "" :
            options[key] = i
            key = ""
            continue

        if i.startswith("-") :
            if "=" in i : key, value = i.split("=")
            else : key = i

            if key in ['-h', '--help'] : print(MANUEL) ; sys.exit(0)
            elif key in ['-i', '--input'] :     key = "filename"
            elif key in ['-o', '--output'] :    key = "output"
            elif key in ['-p', '--pdf-output'] :key = "pdf_output"
            elif key in ['-m', '--md-output'] : key = "md_output"
            elif key in ['-c', '--css-file'] :  key = "css_file"
            elif key in ['-d', '--delete-md'] : options['delete_md'] = True
            else : print(f"Unknow option \"{key}\".") ; sys.exit(1)
            #case of option and value : --input=template.md
            if value != "" :
                options[key] = value
                value, key = "", ""

        else :
            if os.path.exists(i) :
                with open(i, 'r') as file :
                    try : options["json_data"] = json.load(file)
                    except : print("Les données json sont malformées.")
            else :
                try : options["json_data"] = json.loads(i)
                except : print("Les données json sont malformées.")

    #no input template
    if options['filename'] is None : parser(["-h"])
    #no output name, else set good name
    if options['output'] == "" and options['md_output'] is None :
        print("Invalid output name")
        sys.exit(1)
    else :
        if options['md_output'] is None :
            options['md_output'] = f"{options['output']}.md"
        if options['pdf_output'] is None :
            options['pdf_output'] = f"{options['output']}.pdf"

    return options

MANUEL = """Usage : python3 mdinja.py -i [template_path.md] {options} "[json data or file]"

Example :
python3 markdown_jinja.py -i template.md -o "output_name" "ex_data.json"
python3 markdown_jinja.py -i template.md '{"title": "coucou", "code": true, "line_code": "result = 2+2", "content": "Ceci est le contenu du document !"}'

Options :
    -h | --help : show this manuel.
    -o | --output : set same output filename for markdown and pdf (default : output).
                    ex: for example, "output" gives output.md and output.pdf.
    -p | --pdf-output : set output pdf filename (default : same as md rendered but in .pdf). Has priority over -o options.
    -m | --md-output : set output markdown filename. Has priority over -o options.
    -c | --css-file : set css file for style of rendered pdf.
    -d | --delete-md : after converting markdown in pdf, delete redered markdown.

Available css :
 - github
 - air
 - modest
 - retro
 - splendor
 - or use your own by set path to it."""


def main() :
    options = parser(sys.argv[1:])

    if isinstance(options["json_data"], list) and len(options["json_data"]) > 1 :
        build_multiple(options)

    else :
        #redering...
        result = rendering_engine(options['filename'],
                                  options['json_data'],
                                  md_output=options['md_output'] )
        #...and converting
        md_to_pdf(options['md_output'],
                  pdf_output=options['pdf_output'],
                  css_file=options['css_file'],
                  delete_md=options['delete_md'] )


if __name__ == "__main__" :
    cli = False #mode cli or mode python

    #example of python usage
    if not cli :
        filename = "template.md"
        data = {
            'title' : "This my title",
            'code' : True,
            'line_code' : "result = 3+6",
            'content' : "Content of my article",
        }

        result = rendering_engine(filename, data, md_output="output.md")
        md_to_pdf("output.md", css_file="modest")

    #for cli usage
    if cli :
        main()
